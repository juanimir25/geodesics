## Copyright (C) 2018 hugo
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} RK4 (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: hugo <hugo@HUGOLENOVO>
## Created: 2018-11-17
function [X] = RK4 (X0,n,h)
  
function xdot=f(t,x)
xdot(1)=x(2);
xdot(2)=-2*x(3)*x(2)*x(4)/(x(1)^2+x(3)^2+1);
xdot(3)=x(4);
xdot(4)=-2*x(1)*x(2)*x(4)/(x(1)^2+x(3)^2+1);
endfunction

X=zeros(n,4);
X(1,:)=X0;
t=0;
i=1;
while (i<=n-1) 
  k1=h*feval('f',t,X(i,:));
  k2=h*feval('f',t+h/2,X(i,:)+k1/2);
  k3=h*feval('f',t+h/2,X(i,:)+k2/2);
  k4=h*feval('f',t+h,X(i,:)+k3);
  X(i+1,:)=X(i,:)+(k1+2*k2+2*k3+k4)/6;
  t=t+h;
  i=i+1;
  endwhile
  return;
endfunction
