## Copyright (C) 2018 hugo
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} trapecio (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: hugo <hugo@NEGRITA>
## Created: 2018-11-16

function [e,X] = euler_patras (X0,n,h)
X=zeros(n-11,4);
X(1,:)=X0;
i=1;
e=0;
while i<= n-1
     k=1;
     errordes=0.000001 ;
     ErrIt=1;
     Y=zeros(2,4);
     while (ErrIt>errordes && k<50)
	if (k==1)	
          Y(2,1)=X(i,1)+h*X(i,2);		
          Y(2,2)=X(i,2)+h*(-2*X(i,3)*X(i,2)*X(i,4))/(X(i,1)^2+X(i,3)^2+1);
          Y(2,3)=X(i,3)+h*X(i,4)		;
          Y(2,4)=X(i,4)+h*(-2*X(i,1)*X(i,2)*X(i,4))/(X(i,1)^2+X(i,3)^2+1);
	else
	  Y(2,1)=X(i,1)+h*Y(1,2) ;
	  Y(2,2)=X(i,2)+h*(-2*Y(1,3)*Y(1,2)*Y(1,4))/(Y(1,1)^2+Y(1,3)^2+1);
	  Y(2,3)=X(i,3)+h*Y(1,4);
	  Y(2,4)=X(i,4)+h*(-2*Y(1,1)*Y(1,2)*Y(1,4))/(Y(1,1)^2+Y(1,3)^2+1);
	endif
	k=k+1;
	ErrIt= norm(Y(2,:)-Y(1,:));
  Y(1,:)=Y(2,:);
	endwhile
	X(i+1,:)=Y(1,:);
	if (norm(X(i+1,:)-X(i,:))>e)
 	    e= norm(X(i+1,:)-X(i,:));
	endif
	i=i+1;
endwhile 
return;  
endfunction
