# Metodosnumericos

Código de Octave donde se resuelve mediante el método de Euler hacia adelante, las ecuaciones de las geodésicas sobre el paraboloide hiperbólico.
Grupo: Carrasco, Miranda, Luz, Fuidio
Facultad de Ingeniería de la Universidad de la República