X0=[0,.2,0,-.2];
a=0;
b=2;
n=100;
Compa=zeros(1,7);
Compa2=zeros(1,7);
while n<100000
    h=(b-a)/n;
    
    # C�lculo de la geod�sica usando lsode
    x=lalsode(X0,a,b,n);
    Px=x(:,1).*x(:,3);
    
    #c�lculo de la geod�sica por euler hacia adelante
    [e,X]=euler_pa_del(X0,n,h);
    PX=X(:,1).*X(:,3);

    #c�lculo de la geod�sica usando trapecio
    [et,T]=trapecio(X0,n,h);
    PT=T(:,1).*T(:,3);

    #c�lculo de la geod�sica por euler hacia atras
    [ep,Xp] = euler_patras (X0,n,h);
     Pp=Xp(:,1).*Xp(:,3);

    #c�lculo de la geod�sica por heun
    [eh,Xh] = heun(X0,n,h);
    Ph=Xh(:,1).*Xh(:,3);

    #c�lculo de la geod�sica por Runge Kutta 4
    [XR] = RK4 (X0,n,h);
    PR=XR(:,1).*XR(:,3);
    
    Compa(end+1,:)=[h,n,norm(PX-Px),norm(Pp-Px),norm(PT-Px),norm(Ph-Px), norm(PR-Px)];
    Compa2(end+1,:)=[h,n,norm(X-x),norm(Xp-x),norm(T-x),norm(Xh-x), norm(XR-x)];
    n=n*5
endwhile
