## Copyright (C) 2018 hugo
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} lalsode (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: hugo <hugo@NEGRITA>
## Created: 2018-11-16

function [x] = lalsode (X0,a,b,n)
  
function xdot=f(x,t)
xdot(1)=x(2);
xdot(2)=-2*x(3)*x(2)*x(4)/(x(1)^2+x(3)^2+1);
xdot(3)=x(4);
xdot(4)=-2*x(1)*x(2)*x(4)/(x(1)^2+x(3)^2+1);
endfunction
x=lsode('f',X0,(t=linspace(a,b,n)'));

endfunction
