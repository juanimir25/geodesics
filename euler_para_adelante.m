## Copyright (C) 2018 hugo
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} euler_pa_del (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: hugo <hugo@NEGRITA>
## Created: 2018-11-16

function [e,X]=euler_pa_del(X0,n,h)
X=zeros(n-11,4);
X(1,:)=X0;
i=1;
e=0
while i<= n-1
X(i+1,1)=X(i,1)+h*X(i,2);
X(i+1,2)=X(i,2)+h*(-2*X(i,3)*X(i,2)*X(i,4))/(X(i,1)^2+X(i,3)^2+1);
X(i+1,3)=X(i,3)+h*X(i,4);
X(i+1,4)=X(i,4)+h*(-2*X(i,1)*X(i,2)*X(i,4))/(X(i,1)^2+X(i,3)^2+1);
if (sqrt((X(i+1,1)-X(i,1))^2 + (X(i+1,3)-X(i,3))^2)>e)
 e= sqrt((X(i+1,1)-X(i,1))^2 + (X(i+1,3)-X(i,3))^2);
endif
i=i+1;
endwhile
return
endfunction
